<?php

/**
 * @OA\Info(title="Base Project", version="0.0.1")
 */

/**
 *  @OA\Get(
 *     path="/",
 *     description="Home page",
 *     @OA\Parameter(
 *         name="limit",
 *         in="query",
 *         description="How many items to return at one time (max 100)",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format="int32"
 *         )
 *     ),
 *     @OA\Response(response="default", description="Welcome page")
 * )
 */

/**
 * @OA\SecurityScheme(
 *   securityScheme="bearerAuth",
 *   type="http",
 *	 scheme="bearer",
 *	 bearerFormat="JWT"
 * )
 */

/**
 * @OA\Get(
 *     path="/user",
 *     description="Get User",
 *     @OA\Parameter(
 *         name="limit",
 *         in="query",
 *         description="How many items to return at one time (max 100)",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format="int32"
 *         )
 *     ),
 *     security={{
 *       "bearerAuth":{}
 *     }},
 *     @OA\Response(response="default", description="Welcome page")
 * )
 */


/**
 * @OA\Post(
 *	tags={"Auth"},
 *	path="/api/register",
 *   description="Register user",
 *	@OA\Parameter(
 *	name="email",
 *         in="query",
 *         description="Enter your email",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format="int32"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Enter your name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format="int32"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Enter your password",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format="int32"
 *         )
 *     ),
 *     @OA\Response(response="default", description="Register success")
 * )
 */
